package io.github.robogaming.purgatory;

import net.md_5.bungee.api.ChatColor;
import net.md_5.bungee.api.chat.BaseComponent;
import net.md_5.bungee.api.chat.TextComponent;
import net.md_5.bungee.api.connection.ProxiedPlayer;
import net.md_5.bungee.api.event.ChatEvent;
import net.md_5.bungee.api.event.PlayerDisconnectEvent;
import net.md_5.bungee.api.event.ServerKickEvent;
import net.md_5.bungee.api.plugin.Listener;
import net.md_5.bungee.api.plugin.Plugin;
import net.md_5.bungee.config.Configuration;
import net.md_5.bungee.config.ConfigurationProvider;
import net.md_5.bungee.config.YamlConfiguration;
import net.md_5.bungee.event.EventHandler;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

public final class Purgatory extends Plugin implements Listener {
    Configuration config;
    ArrayList<PlayerGoingToGame> playersGoingToGames = new ArrayList<>();

    @Override
    public void onEnable() {
        // Plugin startup logic
        getProxy().getPluginManager().registerListener(this, this);
        checkConfig();
    }

    @Override
    public void onDisable() {
        // Plugin shutdown logic
    }

    public void checkConfig() {
        try {
            config = ConfigurationProvider.getProvider(YamlConfiguration.class).load(new File(getDataFolder(), "config.yml"));
        } catch (IOException e) {
            if (!getDataFolder().exists())
                getDataFolder().mkdir();

            File file = new File(getDataFolder(), "config.yml");


            if (!file.exists()) {
                try (InputStream in = getResourceAsStream("config.yml")) {
                    Files.copy(in, file.toPath());
                } catch (IOException ex) {
                    e.printStackTrace();
                }
            }
        }
    }

    @EventHandler
    public void onCommand(ChatEvent event) {
        if (event.isCommand() && event.getSender() instanceof ProxiedPlayer) {
            String[] command = event.getMessage().split("/")[1].split(" ");
            String commandName = command[0];
            ProxiedPlayer player = ((ProxiedPlayer)event.getSender());
            switch (commandName) {
                case "hub":
                case "lobby":
                    player.connect(getProxy().getServers().get("lobby1"));
                    PlayerGoingToGame playerGoingToGame = null;
                    for (PlayerGoingToGame p : playersGoingToGames) {
                        if (player == p.player) {
                            playerGoingToGame = p;
                        }
                    }
                    if (playerGoingToGame != null) {
                        playersGoingToGames.remove(playerGoingToGame);
                    }
                    event.setCancelled(true);
                    break;
                case "game":
                    if (config.getList("games").contains(command[1])) {
                        playerGoingToGame = null;
                        for (PlayerGoingToGame p : playersGoingToGames) {
                            if (player == p.player) {
                                playerGoingToGame = p;
                            }
                        }
                        if (playerGoingToGame != null) {
                            playersGoingToGames.remove(playerGoingToGame);
                        }
                        connectToGame(player, command[1]);
                    } else {
                        TextComponent tc = new TextComponent("That game doesn't exist.");
                        tc.setColor(ChatColor.RED);
                        player.sendMessage(tc);
                    }
                    event.setCancelled(true);
                    break;
            }
        }
    }

    public void connectToGame(ProxiedPlayer player, String targetGame) {
        if (config.getList("games").contains(targetGame)) {
            int numberOfServers = 1;
            while (getProxy().getServers().containsKey(targetGame + (numberOfServers++)));
            PlayerGoingToGame playerGoingToGame = new PlayerGoingToGame(player, targetGame, numberOfServers);
            playerGoingToGame.player.connect(getProxy().getServerInfo(targetGame + "1"));
            playersGoingToGames.add(playerGoingToGame);
        }
    }

    @EventHandler
    public void kickedEvent(ServerKickEvent event) { //TODO: Stop using the deprecated getKickReason() and sendMessage()
        if (event.getKickedFrom().getName().equals("lobby1")) {
            TextComponent message = new TextComponent("We are out of servers to reconnect you to! Kicked for: ");
            message.setColor(ChatColor.RED);
            message.addExtra(event.getKickReason());
            event.setKickReasonComponent(new BaseComponent[] {message});
        } else {
            event.setCancelled(true);
            PlayerGoingToGame playerGoingToGame = null;
            for (PlayerGoingToGame p : playersGoingToGames) {
                if (event.getPlayer() == p.player) {
                    playerGoingToGame = p;
                }
            }
            if (playerGoingToGame != null && !event.getKickReason().contains("Play again soon!")) {
                if (event.getKickReason().contains("game-restart")) {
                    playersGoingToGames.remove(playerGoingToGame);
                    event.setCancelServer(getProxy().getServers().get("lobby1"));
                    TextComponent tc = new TextComponent("Please wait 5 seconds. You will be sent to the next game automatically.");
                    tc.setColor(ChatColor.GREEN);
                    event.getPlayer().sendMessage(tc);
                    PlayerGoingToGame finalPlayerGoingToGame = playerGoingToGame;
                    getProxy().getScheduler().schedule(this, () -> {
                        connectToGame(finalPlayerGoingToGame.player, finalPlayerGoingToGame.targetGame);
                    }, 5, TimeUnit.SECONDS);
                } else if (playerGoingToGame.onGame < playerGoingToGame.maxNumber) {
                    event.setCancelServer(getProxy().getServers().get(playerGoingToGame.targetGame + (++playerGoingToGame.onGame)));
                } else {
                    event.setCancelServer(getProxy().getServers().get("lobby1"));
                    event.getPlayer().sendMessage(ChatColor.GOLD + ">>> " + ChatColor.ITALIC + ChatColor.WHITE + "That game is full!");
                }
            } else {
                event.setCancelServer(getProxy().getServers().get("lobby1"));
                event.getPlayer().sendMessage(ChatColor.GOLD + ">>> " + ChatColor.ITALIC + ChatColor.WHITE + event.getKickReason());
            }
        }
    }

    @EventHandler
    public void leaveEvent(PlayerDisconnectEvent event) {
        PlayerGoingToGame playerGoingToGame = null;
        for (PlayerGoingToGame p : playersGoingToGames) {
            if (event.getPlayer() == p.player) {
                playerGoingToGame = p;
            }
        }
        if (playerGoingToGame != null) {
            playersGoingToGames.remove(playerGoingToGame);
        }
        event.getPlayer().setReconnectServer(getProxy().getServers().get("lobby1"));
    }
}
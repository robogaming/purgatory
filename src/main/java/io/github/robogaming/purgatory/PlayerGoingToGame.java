package io.github.robogaming.purgatory;

import net.md_5.bungee.api.connection.ProxiedPlayer;

public class PlayerGoingToGame {
    ProxiedPlayer player;
    String targetGame;
    int onGame = 1;
    int maxNumber;

    PlayerGoingToGame(ProxiedPlayer player, String game, int maxNumber) {
        this.targetGame = game;
        this.player = player;
        this.maxNumber = maxNumber;
    }
}
